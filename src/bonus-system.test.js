import {calculateBonuses} from "./bonus-system.js";

describe('Bonus system tests', () => {
    console.log("Tests started");
    let programs = ["Standard", "Premium", "Diamond", "Other"]
    let programsMapping = {
        Standard: 0.05,
        Premium: 0.1,
        Diamond: 0.2,
        Other: 0
    }
    let amountValues = {
        low: [0, 5000, 9999],
        medium: [10000, 25000, 49999],
        high: [50000, 75000, 99999],
        over: [100000, 500000, 1000000]
    }
    let amountMapping = {
        low: 1,
        medium: 1.5,
        high: 2,
        over: 2.5
    }

    test('Standard', (done) => {
        let program = programs[0]
        let programMulti = programsMapping[program]
        for (let amountType in amountValues) {
            let amountMulti = amountMapping[amountType]
            for (let amountValue of amountValues[amountType]) {
                expect(calculateBonuses(program, amountValue)).toEqual(programMulti * amountMulti)
            }
        }
        done();
    });

    test('Premium', (done) => {
        let program = programs[1]
        let programMulti = programsMapping[program]
        for (let amountType in amountValues) {
            let amountMulti = amountMapping[amountType]
            for (let amountValue of amountValues[amountType]) {
                expect(calculateBonuses(program, amountValue)).toEqual(programMulti * amountMulti)
            }
        }
        done();
    });

    test('Diamond', (done) => {
        let program = programs[2]
        let programMulti = programsMapping[program]
        for (let amountType in amountValues) {
            let amountMulti = amountMapping[amountType]
            for (let amountValue of amountValues[amountType]) {
                expect(calculateBonuses(program, amountValue)).toEqual(programMulti * amountMulti)
            }
        }
        done();
    });

    test('Other', (done) => {
        let program = programs[3]
        for (let amountType in amountValues) {
            for (let amountValue of amountValues[amountType]) {
                expect(calculateBonuses(program, amountValue)).toEqual(0)
            }
        }
        done();
    });

    console.log('Tests Finished');

});
